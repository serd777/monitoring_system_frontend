/*
    Контроллер модуля информирования
 */

mainModule.controller('informController', function(
    $scope,
    $rootScope,
    informFactory
) {
    $scope.info_data = {};

    $scope.init = function() {
        updateData();
    };
    $scope.$on('usersUpdated', function(data) {
        $scope.info_data.users = data;
    });

    function updateData() {
        informFactory.getData().then(function(data) {
            $scope.info_data = data;
        });
    }
});