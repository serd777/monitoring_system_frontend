/*
    Редактор текущего аккаунта
 */

mainModule.controller('myAccountController', function(
    $scope,
    $rootScope,
    usersFactory
) {
        $scope.user = {};
        $scope.default = {};

        $scope.init = function() {
            updateInfo();
        };
        $scope.sendData = function() {
            var input = {};

            if($scope.user.login != $scope.default.login)
                input.login = $scope.user.login;
            if($scope.user.name != $scope.default.name)
                input.name = $scope.user.name;
            if($scope.user.password != '')
                input.password = $scope.user.password;

            usersFactory.updateCurrentUser(input).then(function(data) {
                if(data != 'OK')
                    $rootScope.$emit('notifierError', data);
                else {
                    $rootScope.$emit('notifierSuccess', 'Аккаунт успешно обновлен.');
                    $('#modalEditAccount').modal('toggle');

                    updateInfo();
                }
            });
        };

        function updateInfo() {
            usersFactory.currentUserInfo().then(function(data) {
                $scope.user = data;
                $scope.default = angular.copy(data);
            });
        }
    });