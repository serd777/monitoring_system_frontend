/*
    Контроллер управления настройками сайта
 */

mainModule.controller('configController', function(
    $scope,
    configFactory
) {
        $scope.env_data = [];

        $scope.init = function() {
            getEnvData();
        };

        function getEnvData() {
            configFactory.getEnvValues().then(function(data) {
                $scope.env_data = data;
            });
        }
    });