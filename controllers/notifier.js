/*
    Контроллер всплывающего окна (ошибка/успех)
 */

mainModule.controller('notifierController', function(
    $scope,
    $rootScope
) {
        $scope.error_msg   = '';
        $scope.success_msg = '';

        var error_widget   = $('#notify_red');
        var success_widget = $('#notify_green');

        $scope.init = function() {
            error_widget.hide();
            success_widget.hide();
        };
        $rootScope.$on('notifierError', function(event, data) {
            success_widget.hide();
            $scope.error_msg = data;

            error_widget.fadeIn('slow', function() {
                setTimeout(function() {
                    error_widget.fadeOut('slow');
                }, 3000);
            });
        });
        $rootScope.$on('notifierSuccess', function(event, data) {
            error_widget.hide();
            $scope.success_msg = data;

            success_widget.fadeIn('slow', function() {
                setTimeout(function() {
                    success_widget.fadeOut('slow');
                }, 3000);
            });
        });
    });