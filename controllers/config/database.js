/*
    Настройки БД
 */

mainModule.controller('configDbController', function(
    $scope,
    $rootScope,
    configFactory
) {
    $scope.mysql = {};

    $scope.$watch('env_data', function(newValue) {
        $scope.mysql = newValue.mysql;
    });
    $scope.updateSettings = function() {
        configFactory.setDatabaseValues($scope.mysql).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else
                $rootScope.$emit('notifierSuccess', 'Настройки базы данных были успешно обновлены');
        });
    };
});