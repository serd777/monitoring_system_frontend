/*
    Настройки почты
 */

mainModule.controller('configMailController', function(
    $scope,
    $rootScope,
    configFactory
) {
    $scope.mail = {};

    $scope.$watch('env_data', function(newValue) {
        $scope.mail = newValue.mail;
    });
    $scope.updateSettings = function() {
        configFactory.setMailValues($scope.mail).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else
                $rootScope.$emit('notifierSuccess', 'Настройки почтового клиента обновлены!');
        });
    };
});