/*
    Настройки SMS.Ru
 */

mainModule.controller('configSmsController', function(
    $scope,
    $rootScope,
    configFactory
) {
    $scope.sms_key = '';

    $scope.$watch('env_data', function(newValue) {
        $scope.sms_key = newValue.sms_api;
    });
    $scope.updateKey = function() {
        configFactory.setSmsApiKey($scope.sms_key).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else
                $rootScope.$emit('notifierSuccess', 'SMS.Ru API ключ обновлен!');
        });
    };
});