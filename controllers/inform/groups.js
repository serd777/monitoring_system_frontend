/*
    Контроллер информационных групп
 */

mainModule.controller('informGroupsController', function(
    $scope,
    $rootScope,
    informFactory
) {
    $scope.groups = [];
    $scope.users = [];

    $scope.new_group = '';
    $scope.new_member = {};

    $scope.members = [];
    $scope.selected = {};

    var members_modal = $('#modalInformMembers');

    $scope.$watch('info_data', function(newValue) {
        $scope.groups = newValue.groups;
        $scope.users = newValue.users;
    });

    $scope.addGroup = function() {
        informFactory.addGroup($scope.new_group).then(function(answer) {
            if(answer.status != 'SUCCESS')
                $rootScope.$emit('notifierError', answer.data);
            else {
                $scope.new_group = '';
                $scope.groups.push(answer.data);
                $rootScope.$emit('notifierSuccess', 'Информационная группа была успешно добавлена.');
            }
        });
    };
    $scope.deleteGroup = function(group) {
        informFactory.deleteGroup(group.id).then(function() {
            var index = $scope.groups.indexOf(group);
            $scope.groups.splice(index, 1);

            $rootScope.$emit('notifierSuccess', 'Группа успешно удалена.');
        });
    };
    $scope.updateGroup = function(group) {
        informFactory.editGroup(group).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else
                $rootScope.$emit('notifierSuccess', 'Группа была успешно обновлена.');
        });
    };
    $scope.showMembers = function(group) {
        informFactory.getGroupMembers(group.id).then(function(answer) {
            $scope.members = answer.data;
            $scope.selected = group;
            members_modal.modal('show');
        });
    };
    $scope.addMember = function() {
        if(!$scope.new_member.item) {
            $rootScope.$emit('notifierError', 'Выберите пользователя из списка!');
            return;
        }

        informFactory.addToGroup(
            $scope.selected.id,
            $scope.new_member.item.id
        ).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else {
                var member = angular.copy($scope.new_member.item);

                $scope.members.push(member);
                $scope.new_member.item = null;

                $rootScope.$emit('notifierSuccess', 'Пользователь успешно добавлен в группу');
            }
        });
    };
    $scope.deleteMember = function(member) {
        informFactory.deleteFromGroup(
            $scope.selected.id,
            member.id
        ).then(function() {
            var index = $scope.members.indexOf(member);
            $scope.members.splice(index, 1);

            $rootScope.$emit('notifierSuccess', 'Пользователь успешно удален из группы');
        });
    };
});