/*
    Контроллер информационных пользователей
 */

mainModule.controller('informUsersController', function(
    $scope,
    $rootScope,
    informFactory
) {
    $scope.users = [];
    $scope.selected = {};
    $scope.new_user = {};

    var edit_modal = $('#modalInformUser');

    $scope.$watch('info_data', function(newValue) {
        $scope.users = newValue.users;
    });
    $scope.$watch('users', function(newValue) {
        $scope.$emit('usersUpdated', newValue);
    });

    $scope.addUser = function() {
        informFactory.addUser($scope.new_user).then(function(answer) {
            if(answer.status != 'SUCCESS')
                $rootScope.$emit('notifierError', answer.data);
            else {
                $scope.new_user = {};
                $scope.users.push(answer.data);
                $rootScope.$emit('notifierSuccess', 'Информационный пользователь был успешно добавлен.');
            }
        });
    };
    $scope.deleteUser = function(user) {
        informFactory.deleteUser(
            user.id
        ).then(function() {
            var index = $scope.users.indexOf(user);
            $scope.users.splice(index, 1);

            $rootScope.$emit('notifierSuccess', 'Пользователь успешно удален.');
        });
    };
    $scope.updateUser = function() {
        informFactory.editUser($scope.selected).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else {
                edit_modal.modal('hide');
                $rootScope.$emit('notifierSuccess', 'Пользователь был успешно обновлен!');
            }
        });
    };

    $scope.showEditWindow = function(user) {
        $scope.selected = user;
        edit_modal.modal('show');
    };
});