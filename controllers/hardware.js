/*
    Контролер управления устройствами
 */

mainModule.controller('hardwareController', function(
    $scope,
    $rootScope,
    hardwareFactory,
    informFactory
) {
    $scope.devices = [];
    $scope.inform_data = [];

    $scope.new_device = {};
    $scope.new_device.ping_count = 3;
    $scope.new_device.check_period = 1;
    $scope.new_device.inform_period = 1;

    $scope.tree = [];
    $scope.selectedDevice = null;
    $scope.selectedInfo = {};

    $scope.inform_items = [];
    $scope.new_inform = {};

    var addInProgress = false;
    var modal_edit = $('#modalEditDevice');
    var modal_inform = $('#modalInformDevice');

    $scope.init = function() {
        getDevices();
        getInformInfo();
    };
    $scope.toggle = function (scope) {
        scope.toggle();
    };
    $scope.showInfo = function(item) {
        hardwareFactory.getDeviceInfo(item.id).then(function(data) {
            $scope.selectedInfo = data;
            $scope.selectedDevice = item;

            $scope.new_inform.device_id = item.id;
        });
    };
    $scope.addDevice = function() {
        if(addInProgress) {
            $rootScope.$emit('notifierError', 'Дождитесь окончания процесса добавления...');
            return;
        }
        addInProgress = true;

        hardwareFactory.addDevice($scope.new_device).then(function(answer) {
            if(answer.status != 'SUCCESS')
                $rootScope.$emit('notifierError', answer.data);
            else {
                $rootScope.$emit('notifierSuccess', 'Добавлен успешно!');
                $scope.devices.push(answer.data);
                if(!answer.data.parent_id)
                    $scope.tree.push(answer.data);
                else {
                    var parent = getDeviceById(answer.data.parent_id);
                    if(!parent.nodes)
                        parent.nodes = [];
                    parent.nodes.push(answer.data);
                }
            }

            addInProgress = false;
        });
    };
    $scope.deleteDevice = function(device) {
        hardwareFactory.deleteDevice(device.id).then(function() {
            var index = $scope.devices.indexOf(device);
            $scope.devices.splice(index, 1);

            index = $scope.tree.indexOf(device);
            if(index >= 0)
                $scope.tree.splice(index, 1);

            for(var i = 0; i < $scope.devices.length; i++)
                deleteAllCopies($scope.devices[i], device);

            $rootScope.$emit('notifierSuccess', 'Узел был успешно удален!');
        });
    };
    $scope.updateDevice = function() {
        var data = angular.copy($scope.selectedDevice);
        data.ping_count = $scope.selectedInfo.ping_count;
        data.check_period = $scope.selectedInfo.check_period;
        data.inform_period = $scope.selectedInfo.inform_period;

        hardwareFactory.updateDevice(data).then(function(answer) {
            if(answer != 'OK')
                $rootScope.$emit('notifierError', answer);
            else {
                $rootScope.$emit('notifierSuccess', 'Информация об устройстве изменена')
                modal_edit.modal('hide');
            }
        });
    };
    $scope.showEditModal = function() {
        modal_edit.modal('show');
    };
    $scope.showInformModal = function() {
        hardwareFactory.getInformInfo($scope.selectedDevice.id).then(function(data) {
            $scope.inform_items = data;

            for(var i = 0; i < $scope.inform_items.length; i++) {
                $scope.inform_items[i].use_sms = $scope.inform_items[i].use_sms == 1;
                $scope.inform_items[i].use_email = $scope.inform_items[i].use_email == 1;
            }

            modal_inform.modal('show');
        });
    };
    $scope.addInformItem = function() {
        hardwareFactory.addInformInfo($scope.new_inform).then(function(answer) {
            if(answer.status != 'SUCCESS')
                $rootScope.$emit('notifierError', answer.data);
            else {
                $scope.inform_items.push(answer.data);

                $scope.new_inform.item = {};
                $scope.new_inform.email = false;
                $scope.new_inform.phone = false;

                $rootScope.$emit('notifierSuccess', 'Информационная единица добавлена успешно');
            }
        });
    };
    $scope.deleteInformInfo = function(item) {
        hardwareFactory.deleteInformInfo(item.id).then(function() {
            var index = $scope.inform_items.indexOf(item);
            $scope.inform_items.splice(index, 1);

            $rootScope.$emit('notifierSuccess', 'Информационная единица удалена успешно');
        });
    };
    $scope.updateInformInfo = function(item) {
        hardwareFactory.updateInformInfo(item).then(function() {
            $rootScope.$emit('notifierSuccess', 'Информационная единица обновлена');
        });
    };

    $scope.findInformUser = function(id) {
        return $scope.inform_data.filter(function(item) {
            if(item.id == id && item.type == 'user') return item;
        })[0];
    };
    $scope.findInformGroup = function(id) {
        return $scope.inform_data.filter(function(item) {
            if(item.id == id && item.type == 'group') return item;
        })[0];
    };

    function getInformInfo() {
        informFactory.getData().then(function(data) {
            for(var i = 0; i < data.users.length; i++)
                data.users[i].type = 'user';
            for(i = 0; i < data.groups.length; i++)
                data.groups[i].type = 'group';

            $scope.inform_data = $scope.inform_data.concat(data.users);
            $scope.inform_data = $scope.inform_data.concat(data.groups);
        });
    }
    function getDevices() {
        hardwareFactory.getDevices().then(function(data) {
            $scope.devices = data;

            for(var i = 0; i < $scope.devices.length; i++) {
                if(!$scope.devices[i].parent_id)
                    $scope.tree.push($scope.devices[i]);
                else {
                    var parent = getDeviceById($scope.devices[i].parent_id);

                    if(!parent.nodes)
                        parent.nodes = [];

                    parent.nodes.push($scope.devices[i]);
                }
            }

            $scope.$broadcast('angular-ui-tree:collapse-all');
        });
    }
    function getDeviceById(id) {
        return $scope.devices.filter(function(item){
            return item.id === id
        })[0];
    }
    function deleteAllCopies(device, deviceDel) {
        if(device.nodes) {
            var index = device.nodes.indexOf(deviceDel);
            if(index >= 0)
                device.nodes.splice(index, 1);
            for(var i = 0; i < device.nodes.length; i++)
                deleteAllCopies(device.nodes[i], deviceDel);
        }
    }
});