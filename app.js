/*
    Основной модуль приложения
 */

$(function() {

    //Функция, изменяющая размер контейнера
    function resizeWrap() {
        $(".content-wrapper, .right-side").css('min-height', $(window).height());
    }

    //Обработчик изменения размера окна браузера
    $(window).resize(function() {
        //Изменяем минимальную высоту контейнера содрежимого
        resizeWrap();
    });

    //Стартовое значение высоты контейнера
    resizeWrap();
});

//Angular модуль
var mainModule = angular.module('siteApp', [
    'ngSanitize',
    'ui.tree',
    'ui.select',
    'ui.bootstrap'
]);