/*
    Сервис доступа к данным устройств
 */

mainModule.factory('hardwareFactory', function($http) {
        return {
            getDevices : function() {
                return $http.get(
                    '/hardware/list'
                ).then(function(response) {
                    return response.data;
                });
            },

            addDevice : function(input) {
                return $http.post(
                    '/hardware/add',
                    input
                ).then(function(response) {
                    return response.data;
                });
            },

            deleteDevice : function(id) {
                return $http.post('/hardware/delete', {
                    id : id
                }).then(function(response) {
                    return response.data;
                });
            },

            updateDevice : function(input) {
                return $http.post(
                    '/hardware/update',
                    input
                ).then(function(response) {
                    return response.data;
                });
            },

            getDeviceInfo : function(id) {
                return $http.post('/hardware/device_info', {
                    id : id
                }).then(function(response) {
                    return response.data;
                });
            },

            getInformInfo : function(id) {
                return $http.post('/hardware/inform_info', {
                    id : id
                }).then(function(response) {
                    return response.data;
                });
            },

            addInformInfo : function(input) {
                var data = {
                    device_id : input.device_id,
                    group_id  : input.item.type == 'group' ? input.item.id : null,
                    user_id   : input.item.type == 'user'  ? input.item.id : null,
                    phone     : input.phone,
                    email     : input.email
                };

                return $http.post(
                    '/hardware/add_inform',
                    data
                ).then(function(response) {
                    return response.data;
                });
            },

            deleteInformInfo : function(id) {
                return $http.post(
                    '/hardware/del_inform',
                    {id : id}
                ).then(function(response) {
                    return response.data;
                });
            },

            updateInformInfo : function(input) {
                return $http.post(
                    '/hardware/upd_inform',
                    input
                ).then(function(response) {
                    return response.data;
                });
            }
        }
    });