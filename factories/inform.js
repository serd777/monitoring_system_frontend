/*
    Сервис доступа к информационным настройкам
 */

mainModule.factory('informFactory', function($http) {
    return {
        getData : function() {
            return $http.get(
                '/inform/data'
            ).then(function(response) {
                return response.data;
            });
        },

        getGroupMembers : function(id) {
            return $http.post(
                '/inform/group_members',
                { group_id : id }
            ).then(function(response) {
                return response.data;
            });
        },

        addGroup : function(name) {
            return $http.post(
                '/inform/add_group',
                { name : name }
            ).then(function(response) {
                return response.data;
            });
        },

        deleteGroup : function(id) {
            return $http.post(
                '/inform/del_group',
                { id : id }
            ).then(function(response) {
                return response.data;
            });
        },

        editGroup : function(input) {
            return $http.post(
                '/inform/edit_group',
                input
            ).then(function(response) {
                return response.data;
            });
        },

        addToGroup : function(group_id, user_id) {
            return $http.post(
                '/inform/add_to_group',
                {
                    group_id : group_id,
                    user_id  : user_id
                }
            ).then(function(response) {
                return response.data;
            });
        },

        deleteFromGroup : function(group_id, user_id) {
            return $http.post(
                '/inform/del_from_group',
                {
                    group_id : group_id,
                    user_id  : user_id
                }
            ).then(function(response) {
                return response.data;
            });
        },

        addUser : function(input) {
            return $http.post(
                '/inform/add_user',
                input
            ).then(function(response) {
                return response.data;
            });
        },

        deleteUser : function(id) {
            return $http.post(
                '/inform/del_user',
                {id : id}
            ).then(function(response) {
                return response.data;
            });
        },

        editUser : function(input) {
            return $http.post(
                '/inform/edit_user',
                input
            ).then(function(response) {
                return response.data;
            });
        }
    }
});