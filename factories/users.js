/*
    Фабрика для пользователей
 */

mainModule.factory('usersFactory', function($http) {
        return {
            currentUserInfo : function() {
                return $http.get('/users/current/info').then(function(response) {
                    return response.data;
                });
            },

            updateCurrentUser : function(input) {
                return $http.post(
                    '/users/current/update',
                    input
                ).then(function(response) {
                    return response.data;
                })
            }
        }
    });