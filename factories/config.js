/*
    Сервис работы с настройками сайта
 */

mainModule.factory('configFactory', function($http) {
        return{
            getEnvValues : function() {
                return $http.get(
                    '/config/list'
                ).then(function(response) {
                    return response.data;
                });
            },

            setDatabaseValues : function(input) {
                return $http.post(
                    '/config/set_database',
                    input
                ).then(function(response) {
                    return response.data;
                });
            },

            setMailValues : function(input) {
                return $http.post(
                    '/config/set_mail',
                    input
                ).then(function(response) {
                    return response.data;
                });
            },

            setSmsApiKey : function(key) {
                return $http.post('/config/set_sms_ru', {
                    sms_api: key
                }).then(function(response) {
                    return response.data;
                });
            }
        }
    });